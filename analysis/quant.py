import numpy as np
from scipy.integrate import simps

def PSD(signals, n, dt=0.001):
    fhat = fft(signals, n)
    PSD = fhat * np.cont(fhat) / n
    freq = np.arange(n) * (1/(dt*n))
    L = np.arange(1, np.floor(n)/2, dtype='int')
    return freq[L], PSD[L]

dpath = "signals/raw_signals.npy"

data = np.load(dpath)
signals = data[:, :-2]
labels = data[:, -1]

size = signals.shape[1]
ap = list(np.where(labels == 0))
zk = list(np.where(labels == 1))
dg = list(np.where(labels == 2))
cea = list(np.where(labels == 3))

n_classes = len(set(labels))
n_signals = signals.shape[0]
length_signals = size + 4
total_mean = np.mean(signals.flatten())

n_pc = {}
n_pc['aptamero'] = len(signals[ap])
n_pc['zika'] = len(signals[zk])
n_pc['dengue'] = len(signals[dg])
n_pc['cea'] = len(signals[cea])

mean_pc = {}
mean_pc['aptamero'] = round(np.mean(signals[ap]), 2)
mean_pc['zika'] = round(np.mean(signals[zk]), 2)
mean_pc['dengue'] = round(np.mean(signals[dg]), 2)
mean_pc['cea'] = round(np.mean(signals[cea]), 2)

total_std = round(np.std(signals), 2)
std_pc = {}

std_pc['aptamero'] = round(np.std(signals[ap]), 2)
std_pc['zika'] = round(np.std(signals[zk]), 2)
std_pc['dengue'] = round(np.std(signals[dg]), 2)
std_pc['cea'] = round(np.std(signals[cea]), 2)

fsignals = signals.flatten()

total_diff = round(np.sum(np.diff(fsignals)), 2)
diff_pc = {}

diff_pc['aptamero'] = round(np.sum(np.diff(signals[ap].flatten())), 2)
diff_pc['zika'] = round(np.sum(np.diff(signals[zk].flatten())), 2)
diff_pc['dengue'] = round(np.sum(np.diff(signals[dg].flatten())), 2)
diff_pc['cea'] = round(np.sum(np.diff(signals[cea].flatten())), 2)

total_integral = round(simps(fsignals), 2)
integral_pc = {}

integral_pc['aptamero'] = round(np.mean(simps(signals[ap].flatten())), 2)
integral_pc['zika'] = round(np.mean(simps(signals[zk].flatten())), 2)
integral_pc['dengue'] = round(np.mean(simps(signals[dg].flatten())), 2)
integral_pc['cea'] = round(np.mean(simps(signals[cea].flatten())), 2)


data = [n_classes, n_signals, length_signals, total_mean, mean_pc, total_std, std_pc, total_diff, diff_pc, total_integral, integral_pc]
names = ["n_classes", "n_signals", "length_signals", "total_mean", "mean_pc", "total_std", "std_pc", "total_diff", "diff_pc", "total_integral", "integral_pc"]

datadict = {}

for d, name in zip(data, names):
    datadict[name] = d

with open("original-quant.csv", "a") as f:
    for name in names:
        f.write(f"{name}|{datadict[name]}")
        f.write("")
