"""
Script de análise visual
"""
from utils.visualization import plot_signals, scatter_signals, similarity, similarity_plot
from matplotlib import pyplot as plt
import numpy as np

def signal_similarity_plot(signals, labels, forced='completo', title=None, save=False, show=True, fp="assets/visual/total/", fn="heat.png"):
    ord_signals = signals[labels.argsort()]

    if forced=='completo':
        ord_signals = np.concatenate((ord_signals, labels.reshape((len(labels), 1))), axis=1)
    elif forced=='dicotomia':
        labels = np.array([l if l==1 else 0 for l in labels])
        ord_signals = np.concatenate((ord_signals, labels.reshape((len(labels), 1))), axis=1)

    similarity_plot(ord_signals, title=title, save=save, fn=fp+fn)

def multiscatter(signals, labels, methods, complete=True, save=False, fp=None):

    plt.subplot(221)
    scatter_signals(signals, labels, method=methods[0], title=methods[0].upper(), dengue=True, show=False, legend=False, complete=complete)

    plt.subplot(222)
    scatter_signals(signals, labels, method=methods[1], title=methods[1].upper(), dengue=True, show=False, legend=False, complete=complete)

    plt.subplot(223)
    scatter_signals(signals, labels, method=methods[2], title=methods[2].upper(), dengue=True, show=False, legend=False, complete=complete)

    plt.subplot(224)
    scatter_signals(signals, labels, method=methods[3], title=methods[3].upper(), dengue=True, show=False, legend=True, complete=complete)

    if save:
        plt.savefig("assets/visual/partial/"+str(complete).lower())


# The code below is failing and beautiful

# for m, method in enumerate(methods):
#     last = not bool((m + 1) % n_mtds)
#     position= 221 + m
#     plt.subplot(position)
#     scatter_signals(signals, labels, method, dim, method.upper(), last, False)

#loading signals

data = np.load("signals/raw_signals.npy")
updata = np.load("signals/resampled_raw_signals.npy")
ordered_data = data[data[:,-2].argsort()]

#Simple signal plot

signals = data[:, :-2]
labels = data[:, -1]
simple_labels = data[:, -2]

# plot_signals(signals, labels, sep=True)

#Dimensionality reduction scatterplot

methods = ['pca', 'ica', 'tsne', 'mds']
n_mtds = len(methods)
dim=2

# multiscatter(signals, labels, methods, compelte=True, save=True)

#Similarity plot
