"""
Modelo convolicional, segundo a ser desenvolvido. Arquitetura descrita a 
partir de linha 21.

Gabriel Lara @ 04/05/2020
"""
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import numpy as np
from torch.utils.data.sampler import SubsetRandomSampler
from data_processing import ResponseData, data_samplers
from matplotlib import pyplot as plt
from collections import Counter
from tqdm import tqdm
from visualization import confusion_2d, loss_plot

def demo():
    
    example_input = data.X[0].view(1, 1, 76)
    
    net = ConvNN()
    
    net.forward(example_input.float())
    
device = torch.device('cpu')

#TODO: estudar instabilidade do modelo

class ConvNN(nn.Module):
    
    def __init__(self):
        
        super(ConvNN, self).__init__()
        self.final_out_dim = 280
        self.hid_1 = int(self.final_out_dim*1.5)
        self.hid_2 = int(self.final_out_dim*2)
        self.pool = nn.MaxPool1d(4)
        self.conv1 = nn.Conv1d(in_channels=1, out_channels=3, kernel_size=8)
        self.conv2 = nn.Conv1d(in_channels=3, out_channels=3, kernel_size=3)
        self.conv3 = nn.Conv1d(in_channels=3, out_channels=5, kernel_size=3)
        self.conv4 = nn.Conv1d(in_channels=5, out_channels=5, kernel_size=10)
        self.fc1 = nn.Linear(self.final_out_dim, self.hid_1)
        self.fc2 = nn.Linear(self.hid_1, self.hid_2)
        self.fc3 = nn.Linear(self.hid_2, 2)


    def forward(self, x):
        
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        x = F.relu(self.conv4(x))
        # print(x.shape)
        x = x.view(-1, self.final_out_dim)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        pre_y_hat = self.fc3(x)
      
        return pre_y_hat


data = ResponseData("new_upsignals.npy")

batch_size = 64    
test_split = 0.2    
shuffle_data = True
r_seed= 42

train_sampler, test_sampler = data_samplers(data.n_samples)

train_loader = torch.utils.data.DataLoader(data, batch_size=batch_size, 
                                           sampler=train_sampler)
test_loader = torch.utils.data.DataLoader(data, batch_size=batch_size,
                                          sampler=test_sampler)

n_epochs = 1000
lrate = 1e-4

train = True

#visualização

visual = True
loss_tracker = []

#instancia de modelo
r_seed= 42

if train: neural_net = ConvNN().to(device)

#loss e otimizador

criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(neural_net.parameters(), lr = lrate)
n_total_steps = len(train_loader)

#treinamento
if train:
    
    for epoch in tqdm(range(n_epochs)):
        
        for i, (signals, labels) in enumerate(train_loader):
    
            signals = signals.view(signals.shape[0], 1, data.n_features)
            #forward pass
            signals = signals.to(device)
            labels = labels.to(device)
            
            outputs = neural_net(signals.float())
            loss = criterion(outputs, labels.long())
            #backpropagation
            
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            loss_tracker.append(round(loss.item(), 2))
            
            # if(i + 1 ) % 7 == 0:
            #     print(f'epoch {epoch+1} / {n_epochs}, loss={loss.item():.4f}')
        
#teste 

class_dict = {
              (0, 0): 'tn',
              (0, 1): 'fp',
              (1, 0): 'fn',
              (1, 1): 'tp',
              }

cases = []

with torch.no_grad():
    n_correct = 0
    n_samples = 0
    for signals, labels in test_loader:
        
        signals = signals.view(signals.shape[0], 1, data.n_features)

        signals = signals.to(device)
        labels = labels.to(device)
        outputs = neural_net(signals.float())
        # max returns (value ,index)
        _, predicted = torch.max(outputs.data, 1)
        n_samples += labels.size(0)
        n_correct += (predicted == labels).sum().item()
        
        for y, y_hat in zip(labels.numpy(), predicted.numpy()):
            cases.append(class_dict[(y, y_hat)])
        
case_counter = Counter(cases)   

if visual:
    loss_plot(loss_tracker)
    confusion_2d(case_counter, n_cases=len(cases))
    
case_counter = Counter(cases)   

precision = case_counter['tp'] / (case_counter['tp'] + case_counter['fp'])
recall = case_counter['tp'] / (case_counter['tp'] + case_counter['fn'])

if case_counter['tp'] != 0:
    F1 = 2 * (precision*recall)/(precision+recall)    
    print(f'F1: {F1:.2f}')
    
print(f'precision: {precision:.2f}')
print(f'recall: {recall:.2f}')