"""
Modelo simples (FFNN), primeiro a ser desenvolvido. Arquitetura descrita a 
partir de linha 10.

Gabriel Lara @ 27/04/2020
"""
import torch
import torch.nn as nn

class FeedForwardNN(nn.Module):
    def __init__(self, in_size, hidd_1_s, hidd_2_s, hidd_3_s, hidd_4_s, hidd_5_s, hidd_6_s, out_s):
        
        super(FeedForwardNN, self).__init__()
        
        self.k = hidd_6_s
        self.expose = False
        
        #self.prenet = nn.RNN(in_size, in_size)
        self.l1 = nn.Linear(in_size, hidd_1_s)
        self.l2 = nn.Linear(hidd_1_s, hidd_2_s)
        self.l3 = nn.Linear(hidd_2_s, hidd_3_s)
        self.l4 = nn.Linear(hidd_3_s, hidd_4_s)
        self.l5 = nn.Linear(hidd_4_s, hidd_5_s)
        self.l6 = nn.Linear(hidd_5_s, hidd_6_s)
        self.l7 = nn.Linear(hidd_6_s, hidd_6_s)

        self.outp = nn.Linear(hidd_6_s, out_s)
        self.relu = nn.ReLU()
    
    def forward(self, x):
        
        #x = self.prenet(x)
        x_1 = self.relu(self.l1(x))
        x_2 = self.relu(self.l2(x_1))
        x_3 = self.relu(self.l3(x_2))
        x_4 = self.relu(self.l4(x_3))
        x_5 = self.relu(self.l5(x_4))
        x_6 = self.relu(self.l6(x_5))
        x_7 = self.relu(self.l7(x_6))
        
        if self.expose == True:

            return x_7.detach().numpy()
            
        y_hat = self.outp(x_7)
        
        return y_hat
