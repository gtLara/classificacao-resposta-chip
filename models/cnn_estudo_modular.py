"""
Modelo convolucional modular, separando entre MLP e extração de
de característiacs para verificar se os sinais estão sendo bem
representados (ou como estão sendo representados). A MLP usada 
é idêntica em estrutura ao primeiro modelo desenvolvido.

Gabriel Lara @ 08/05/2020
"""

import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import numpy as np
from torch.utils.data.sampler import SubsetRandomSampler
from data_processing import ResponseData, data_samplers
from matplotlib import pyplot as plt
from collections import Counter
from tqdm import tqdm
from visualization import confusion_2d, loss_plot

def demo():
    
    example_input = data.X[0].view(1, 1, 76)
    
    net = ConvNN()
    
    net.forward(example_input.float())
    
device = torch.device('cpu')

class ConvNN(nn.Module):
    
    def __init__(self):
        
        super(ConvNN, self).__init__()
        
        in_size = 60
        self.in_size = in_size
        self.pool = nn.MaxPool1d(4)
        self.conv1 = nn.Conv1d(in_channels=1, out_channels=3, kernel_size=8)
        self.conv2 = nn.Conv1d(in_channels=3, out_channels=3, kernel_size=10)
        self.conv3 = nn.Conv1d(in_channels=3, out_channels=5, kernel_size=10)
        self.conv4 = nn.Conv1d(in_channels=5, out_channels=5, kernel_size=10)
        
        self.l1 = nn.Linear(in_size, int(in_size*1.5))
        self.l2 = nn.Linear(int(in_size*1.5), int(in_size*1.8))
        self.l3 = nn.Linear(int(in_size*1.8), int(in_size*2))
        self.l4 = nn.Linear(int(in_size*2), int(in_size*2.3))
        self.outp = nn.Linear(int(in_size*2.3), 2)
        
        self.relu = nn.ReLU()


    def forward(self, x):
        
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = F.relu(self.pool(self.conv3(x)))
        #x = F.relu(self.conv4(x))
        #print(x.shape)
        x = x.view(-1, self.in_size)
        
        x_1 = self.relu(self.l1(x))
        x_2 = self.relu(self.l2(x_1))
        x_3 = self.relu(self.l3(x_2))
        x_4 = self.relu(self.l4(x_3))
        
        pre_y_hat = self.outp(x_4)
      
        return pre_y_hat


data = ResponseData("new_upsignals.npy")

demo()

batch_size = data.X.shape[0] #todas amostras
test_split = 0.2    
shuffle_data = True
r_seed = 42

train_sampler, test_sampler = data_samplers(data.n_samples)

train_loader = torch.utils.data.DataLoader(data, batch_size=batch_size, 
                                           sampler=train_sampler)
test_loader = torch.utils.data.DataLoader(data, batch_size=batch_size,
                                          sampler=test_sampler)

n_epochs = 10000
lrate = 1e-4

train = True

#visualização

visual = True
loss_tracker = []

#instancia de modelo
r_seed= 42

if train: neural_net = ConvNN().to(device)

#loss e otimizador

criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(neural_net.parameters(), lr = lrate)
n_total_steps = len(train_loader)

#treinamento
if train:
    
    for epoch in tqdm(range(n_epochs)):
        
        for i, (signals, labels) in enumerate(train_loader):
    
            signals = signals.view(signals.shape[0], 1, data.n_features)
            #forward pass
            signals = signals.to(device)
            labels = labels.to(device)
            
            outputs = neural_net(signals.float())
            loss = criterion(outputs, labels.long())
            #backpropagation
            
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            loss_tracker.append(round(loss.item(), 2))
            
            # if(i + 1 ) % 7 == 0:
            #     print(f'epoch {epoch+1} / {n_epochs}, loss={loss.item():.4f}')
        
#teste 

class_dict = {
              (0, 0): 'tn',
              (0, 1): 'fp',
              (1, 0): 'fn',
              (1, 1): 'tp',
              }

cases = []

#teste

with torch.no_grad():
    n_correct = 0
    n_samples = 0
    for signals, labels in test_loader:
        
        signals = signals.view(signals.shape[0], 1, data.n_features)

        signals = signals.to(device)
        labels = labels.to(device)
        outputs = neural_net(signals.float())
        # max returns (value ,index)
        _, predicted = torch.max(outputs.data, 1)
        n_samples += labels.size(0)
        n_correct += (predicted == labels).sum().item()
        
        for y, y_hat in zip(labels.numpy(), predicted.numpy()):
            cases.append(class_dict[(y, y_hat)])
        
case_counter = Counter(cases)   

if visual:
    loss_plot(loss_tracker)
    confusion_2d(case_counter, n_cases=len(cases))
    
case_counter = Counter(cases)   

precision = case_counter['tp'] / (case_counter['tp'] + case_counter['fp'])
recall = case_counter['tp'] / (case_counter['tp'] + case_counter['fn'])

if case_counter['tp'] != 0:
    F1 = 2 * (precision*recall)/(precision+recall)    
    print(f'F1: {F1:.2f}')
    
print(f'precision: {precision:.2f}')
print(f'recall: {recall:.2f}')

#TODO: estudar mais convoluções