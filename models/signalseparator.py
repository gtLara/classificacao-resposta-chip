import torch.nn as nn
import torch
from scipy.spatial.distance import euclidean as dist
from tqdm import tqdm
import numpy as np

class Encoder(nn.Module):
    def __init__(self, in_size, hidd_1_s, hidd_2_s, hidd_3_s, hidd_4_s,k, decay=5e-2):

        super(Encoder, self).__init__()

        self.k = k
        self.decay = decay

        self.prenet = nn.Linear(in_size, in_size)
        self.l1 = nn.Linear(in_size, hidd_1_s)
        self.l2 = nn.Linear(hidd_1_s, hidd_2_s)
        self.l3 = nn.Linear(hidd_2_s, hidd_3_s)
        self.l4 = nn.Linear(hidd_3_s, hidd_4_s)
        self.postnet = nn.Linear(hidd_4_s, k)
        self.relu = nn.ReLU()

    def supervised_reshape(self, X, y):

        n_class = len(set(y.cpu().numpy()))
        n_spc = int(X.shape[0]/n_class)
        n_features = X.shape[1]
        new_X = torch.zeros((n_class, n_spc, n_features))

        for c in range(n_class):

            indexes = np.where(y.cpu() == c)
            new_X[c] = X[indexes][:n_spc]

        return new_X

    def forward(self, x):

        x = self.relu(self.prenet(x))
        x = self.relu(self.l1(x))
        x = self.relu(self.l2(x))
        x = self.relu(self.l3(x))
        x = self.relu(self.l4(x))
        x = self.postnet(x)
        x = x / torch.norm(x, dim=1, keepdim=True)

        return x

    def train(self, n_epochs, train_loader, criterion,
              device=torch.device('cpu')):

        optimizer = torch.optim.Adam(self.parameters(),
                                    lr = 1e-3,
                                    weight_decay=self.decay)

        loss_tracker = []

        criterion = criterion
        optimizer = optimizer

        for epoch in tqdm(range(n_epochs)):

            for i, (signals, labels) in enumerate(train_loader):

                #forward pass
                signals = signals.to(device)

                for l, label in enumerate(labels):
                    labels[l] = float(label == 1)

                labels = labels.to(device)

                outputs = self.forward(signals.float()).to(device)
                
                outputs = self.supervised_reshape(outputs, labels)
                loss = criterion(outputs)

                #backpropagation

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
                loss_tracker.append(round(loss.item(), 2))

        self.loss_tracker = loss_tracker

class Classifier(nn.Module):
    def __init__(self, in_size, hidd_1_s, hidd_2_s, out):

        super(Classifier, self).__init__()

        self.l1 = nn.Linear(in_size, hidd_1_s)
        self.l2 = nn.Linear(hidd_1_s, hidd_2_s)
        self.out = nn.Linear(hidd_2_s, out)
        self.relu = nn.ReLU()

    def forward(self, x):

        x = self.relu(self.l1(x))
        x = self.relu(self.l2(x))
        x = self.out(x)

        return x

class Verifier():
    def __init__(self, encoder, centroid=None, thresh=None, vclass_name="zika"):
        self.encoder = encoder
        self.threshold = thresh
        self.vclass_name = vclass_name
        self.vcentroid = centroid

    def verify(self, embedding=None, signal=None):

        if embedding is None:
            if signal is None:
                print("No signal to verify!")
            else:
                embedding = self.encoder(signal.float())

        if self.threshold is not None:

            embedding = embedding.detach().numpy()
            distance = dist(embedding, self.vcentroid)

            decision = distance < self.threshold

        else:
            print("Treshold not defined")
            return

        return decision

class SignalSeparator(nn.Module):

    def __init__(self, in_size, e_h1, e_h2, e_h3, e_h4, k, v_h1, v_h2, out):

        super(SignalSeparator, self).__init__()

        self.k = k
        self.encoder = Encoder(in_size, e_h1, e_h2, e_h4, k)
        self.classifier = Verifier()

    def forward(self, x):

        x = self.encoder(x)
        x = self.verifier(x)

        return x