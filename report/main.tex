\documentclass{article}
\usepackage{xcolor}
\usepackage[brazil]{babel}
\usepackage{float}
\usepackage{amsmath}
\usepackage{draftwatermark}
\SetWatermarkText{Confidencial}
\SetWatermarkScale{4}

\begin{document}

\begin{titlepage}
\title{Relatório: Análise de Dados e Desenvolvimento de Método de Classificação
para Sinais de Resposta\\
\vspace{1cm}
\normalsize{Experimentos de Visualização, Análise exploratória de dados,
Elaboração de arquiteturas neurais e validação sobre dados de teste.}}
\vspace{1cm}

\end{titlepage}

\author{Gabriel Teixeira Lara Chaves}
\date{2019}

\maketitle
\pagebreak

\tableofcontents
\pagebreak

\section{Prefácio}

Este relatório tem por objetivo documentar o trabalho desenvolvido pelo autor
no contexto do projeto dos Aptâmeros (CDTN/UFMG). As análises, experimentos e
discussões foram conduzidas de forma independente e representam os pensamentos
e conclusões apenas do autor.

Seu texto pressupõe que o leitor tenha conhecimento sobre o problema geral de
classificação dos sinais de reposta no contexto do projeto assim como noções
básicas de funcionamento de redes neurais e métodos de redução de
dimensionalidade, apresentando explicações técnicas que não explicitam os
mecanismos mais fundamentais das soluções discutidas.

\section{Introdução}

As soluções discutidas nesse documento tem por objetivo explorar o problema de
classificação dos sinais de corrente de resposta do sistema eletrônico
desenvol- vido pelo projeto dos Aptâmeros à uma onda quadrada de tensão. A
função de transferência dos aptâmeros é a principal função estudada no processo
de classificação, apesar das soluções aqui discutidas não abordarem sua
modelagem direta, como observado em alguns trabalhos clássicos
\cite{nason2002}.

O sistema gera saídas muito semelhantes entre si no caso controle, onde não há
condicionamento de nenhuma substância externa. Sob condicionamento de
substâncias externas, espera-se que sua função de transferência seja alterada
de forma consistente, permitindo que a substância condicionadora seja
identificada a partir da corrente de resposta.

A classificação a princípio envolve $N$ classes, com esse valor variando de
acordo com a base de dados analisada. As classes representam os SRs gerados sob
diferentes substâncias e o SR não condicionado. A tarefa do classificador
consiste então em assinalar um sinal de entrada $\mathbf{x}$, arbitrário e
desconhecido, à uma classe específica $y$, correspondente à substância (ou
ausência de substância) condicionadora da medida que gerou $\mathbf{x}$.
Compreende-se a função $y(\mathbf{x})$ aproximada pelo classificador como um
sistema separado do processo de medição dos sinais de reposta que modela
implicitamente a função de transferência global modelando sua função inversa,
uma vez que a função $g(y)$ de transferência gera $\mathbf{x}$ e o
classificador $f(x)$ gera, idealmente, $y$.

Observa-se que o processo de classificação relaciona $\mathbf{x}$ a $y$ de
forma a configurar um mapeamento $N:1$. Apesar da multitude de classes, a
decisão é essencialmente uma dicotomia, retornando verdadeiro ou falso para a
identidade de $\mathbf{x}$. Apesar de ser possível desenvolver um classificador
$N:N$ esse documento abordará o caso dicotômico, referindo-se aos dados com $N$
rótulos de classes como "dados completos" e aos mesmos dados com $2$ rótulos
(positivo e negativo) como "dados dicotômicos".

Os métodos aqui discutidos para desenvolvimento do classificador são baseados
em redes neurais. Como detalhado nas seções correspondentes, os experimentos
foram realizados com bases de dados muito restritas (~$100$ amostras),
quantidade bem inferior ao que pode ser considerado apropriado para as
estruturas estudadas (~$10^{5}$). Assume-se por esse motivo que o desempenho
relatado na seção ~\ref{sec:exp} é inferior ao que se espera no caso de uma
base de dados mais representativa.

\section{Análise Exploratória de Dados}

Interpreta-se de maneira satisfatória a tarefa delegada ao classificador como
uma simples distinção dos sinais de entrada. Nesse contexto, quanto mais
distintos os sinais estão antes de serem submetidos ao sistema melhor. As bases
de dados recebidas foram organizadas e medidas com diferentes métodos em
tentativas de tornar os sinais mais distinguíveis. Tal avaliação depende de uma
maneira confiável de confirmar que um grupo de sinais é mais ou menos
distinguível que outro. As maneiras mais intuitivas de realizar essa
verificação (como uma simples análise dos gráficos dos sinais) podem prejudicar
decisões importantes. Tornam-se necessárias análises quantitativas e visuais
dos dados afim de tornar esse processo mais confiável.

%Torna-se necessário uma metodização desse
% procedimento afim de torná-lo mais certeiro.

Até a redação desse documento foram analisadas duas bases de dados distintas.

A primeira base de dados recebida possui três tempos de bloqueio e quatro
organizações distintas, cada subconjunto de bloqueio variando entre quatro
métodos de normalização.  A segunda base de dados recebida possui apenas um
tempo de bloqueio (duas horas) e as mesmas quatro organizações. O número total
de subconjuntos é $16$. As análises quantitativas e visuais, assim como os
experimentos, não foram relatados sobre todos os subconjuntos por uma série de
motivos. Inicialmente algumas das maneiras de normalização dos dados são
semelhantes entre si a ponto de tornar visualizações e comparações
quantitativas separadas redundantes. Além disso, a diferença entre as
organizações são relativas à transformações lineares dos sinais originais,
processos que são parte natural do processo de aprendizagem neural. Delega-se
em um primeiro instante de exploração do problema tais transformações à rede.
Optou-se por analisar apenas o subconjunto sem tratamento. A mesma observação
se aplica aos resultados dos experimentos. Fora isso, o número de classes entre
os conjuntos varia ora entre três (Zika, CEA, Aptâmero) e quatro (Dengue
adicional).

\color{red}Observa-se que a a decisão de usar os dados não normalizados é importante: tal
prática é comum em problemas semelhantes por geralmente "facilitar" o processo
de aprendizagem do classificador. Uma pré rede será empregado afim de permitir
que a rede principal decida qual forma de normalização será empregada. Essa
rede operará linearmente, sem funções de ativação. Espera-se os resultados
apresentados sejam melhorados com pré normalização.\color{black}

Dessa maneira analisa-se nesta seção o subconjunto mais complexo em termos de
diversidade de classes, sendo esse os sinais medidos sem bloqueio. As seções
seguintes se encarregam de analisar visual e quantitativamente os subconjuntos
mencionados.

Nota-se que comparações com o conjunto com duas horas de bloqueio serão feitas
afim de ilustrar alguns métodos de análise.

\subsection{Análise Quantitativa}

As grandezas analisadas são ilustradas na tabela ~\ref{tab:null}.

\begin{table}[H]
\caption{Grandezas de subconjunto sem bloqueio}
\begin{tabular}{|l|l|l|l|l|l|}
\hline
\textbf{Métricas} & \textbf{Total} & \textbf{Aptâmero} & \textbf{Zika} & \textbf{Dengue} & \textbf{CEA} \\ \hline
\textbf{Número de Sinais}      & 117    &       &       &        &        \\ \hline
\textbf{Comprimento de Sinais} & -      & 80    & 80    & 80     & 80     \\ \hline
\textbf{Média}                 & 0.1    & 0.11  & 0.08  & 0.09   & 0.12   \\ \hline
\textbf{Desvio Padrão}         & 0.05   & 0.05  & 0.03  & 0.04   & 0.05   \\ \hline
\textbf{Soma Derivada}         & -0.25  & -0.41 & -0.22 & -0.11  & -0.04  \\ \hline
\textbf{Média Integral}        & 893.43 & 51.35 & 139.8 & 161.97 & 223.08 \\ \hline
\end{tabular}
\label{tab:null}
\end{table}

Nota-se que algumas medidas aparentam distinguir relativamente bem os sinais,
como a média, onde os valores para Zika e Dengue e Aptâmero e CEA são próximos
entre si, respectivamente. Observa-se também distinção interclasse interessante
na soma das derivadas e média das integrais.

A distinção comunicada por essas grandezas, no entanto, não são o suficiente
para uma boa classificação dos sinais. Sua informação é útil não obstante e
será referida ao longo desse documento como características clássicas, usada em
união ao processo mais avançado de extração de características que será
desenvolvido.

\subsection{Análise Visual}

As imagens elaboradas abordam o processo de visualização dos dados de três
maneiras distintas e complementares.

A primeira delas simplesmente ilustra a "forma original" dos sinais, isso é, os
valores de corrente em função do tempo da forma como foram medidos. As
visualizações são geradas por sobreposição e distintas pelas cores das curvas e
em imagens separadas por classe. Refere-se a essas visualizações como simples.

As duas outras maneiras são bem mais confiáveis por representarem visualmente
abstrações matemáticas dos dados. Visualiza-se uma "forma mastigada" dos
sinais. A primeira dessas abordagens usa de métodos de redução de
dimensionalidade para gerar gráficos de dispersão em duas dimensões. Usa-se
quatro métodos de maneira a preservar informações diferentes do espaço original
de características. A segunda abordagem visa preservar totalmente a topologia
original, ilustrando matrizes de semelhança calculadas sob distância euclidiana
par a par. Refere-se a essas visualizações como parcialmente preservativas e
preservativas, respectivamente.

A maior utilidade dessas visualizações é para comparar diferentes
conjuntos de sinais afim de decidir qual é mais distinguível.

\subsubsection{Visualizações Simples}

Nota-se nas figuras certas características visualmente distinguíveis das
classes, como por exemplo uma maior variância dos valores dos sinais
correspondentes ao aptâmero puro e condicionamento com CEA e uma média maior,
comunicada por uma "altura" maior dos sinais nos gráficos, das classes Zika e
Dengue. Tais observações são de certa forma redundantes, sendo comunicadas na
tabela \ref{tab:null}.

A imagem \ref{fig:simple} demonstra certa separação dos sinais, um bom
presságio para a tarefa eminente de classificação.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{../assets/visual/simple/heat.png}
    \caption{Sinais Sobrepostos}
    \label{fig:simple}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{../assets/visual/simple/Denguesignals.png}
    \caption{Sinais da classe Dengue}
    \label{fig:Dengue}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{../assets/visual/simple/Zikasignals.png}
    \caption{Sinais da classe Zika}
    \label{fig:Zika}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{../assets/visual/simple/CEAsignals.png}
    \caption{Sinais da classe CEA}
    \label{fig:CEA}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{../assets/visual/simple/Aptamerosignals.png}
    \caption{Sinais da classe Aptâmero}
    \label{fig:Aptamero}
\end{figure}

\subsubsection{Visualizações Parcialmente Preservativas}

As visualizações abordadas nessa seção usam de quatro métodos de redução de
dimensionalidade para visualizar os sinais de resposta, sendo eles PCA, ICA,
TSN-E e MDS. Trata-se cada sinal como um vetor de $80$ dimensões. O uso de
múltiplos métodos permite preservar informações diferentes da topologia
original dos dados, como distância entre sinais (estrutura global) no caso de
TSN-E, distância entre pares de sinais (estrutura local) no caso de MDS,
variância no caso de PCA e independência de características do caso de ICA.

O uso dessas diferentes reduções complementa a análise de forma a capacitar o
analista a julgar melhor qual conjunto de sinais é mais distinguível. As
imagens \ref{fig:false} e \ref{fig:true} representam o conjunto de
dados sem bloqueio.

As imagens \ref{fig:comp-1} e \ref{fig:comp-2} usam do processo de visualização
desenvolvido para comparar a separabilidade dos respectivos sinais completos,
exemplificando um de seus possíveis usos.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{"../assets/visual/partial/false.png"}
    \caption{Visualização parcialmente preservativa de dados dicotômicos sem bloqueio}
    \label{fig:false}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{"../assets/visual/partial/true.png"}
    \caption{Visualização parcialmente preservativa de dados completos sem bloqueio}
    \label{fig:true}
\end{figure}

% \begin{figure}[H]
%     \centering
%     \includegraphics{"../assets/visual/old/up-comp-1.png"}
%     \caption{Exemplo de comparação}
%     \label{fig:comp-1}
% \end{figure}

% \begin{figure}[H]
%     \centering
%     \includegraphics{"../assets/visual/old/up-comp-2.png"}
%     \caption{Exemplo de comparação}
%     \label{fig:comp-2}
% \end{figure}


\subsubsection{Visualizações Preservativas}

Afim de preservar totalmente o espaço original dos sinais, onde cada sinal
representa um vetor distinto, emprega-se a construção de matrizes de semelhança.
Cada posição dessa matriz representa a distância euclidiana entre dois sinais
no espaço vetorial e sua diagonal é nula, uma vez que a distância entre um sinal
e si mesmo é zero. Os sinais são ordenados por classe de forma que em uma base
muito distinguível forma-se submatrizes nítidas que as representam, como na
figura \ref{fig:complete-heat}.

Como observado na figura \ref{fig:natural} os sinais originais não comunicam
semelhança o suficiente se dispostos vetorialmente em suas características
naturais para que as submatrizes indicativas das classes sejam visíveis. Isso
pode prejudicar um analista que deseja comparar duas bases distintas afim de
decidir qual possui os sinais mais separados. Para contornar esse aspecto
caótico do posicionamento dos dados usa-se métodos de separação forçada.

Com os rótulos de cada sinal disponíveis, como é o caso para qualquer conjunto
medido, é possível gerar sua matriz de semelhança correspondente incorporando
o rótulo no sinal. Tal "vazamento de dados" faz com que as sub matrizes se
tornem mais visíveis. Espera-se que o ganho de visibilidade absoluta permita
discernir qual dos conjuntos analisados possui mais visibilidade relativa, como
a comparação da imagem \ref{fig:comparison} comunica.

Esse processo de aumento dos dados pode ser executado sobre os rótulos completos
ou dicotômicos, gerando as figuras observadas nas imagens \ref{fig:complete-heat}
e \ref{fig:dict-heat} respectivamente.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{"../assets/visual/total/normal.png}
    \caption{Matriz de similaridade natural}
    \label{fig:natural}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{"../assets/visual/total/completo.png}
    \caption{Matriz de similaridade completa}
    \label{fig:complete-heat}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.5]{"../assets/visual/total/dicotomia.png"}
    \caption{Matriz de similaridade dicotômica}
    \label{fig:dict-heat}
\end{figure}


\subsection{Discussão}

\bibliographystyle{ieeetr}
\bibliography{refs}

\end{document}
