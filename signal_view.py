from utils.visualization import plot_signals, scatter_signals, similarity_plot
import numpy as np
import matplotlib.pyplot as plt

def comparison_splot(nll, blk, complete=0, mth_set=0, write=False, up_status="up"):

    if mth_set == 1:
        mth1='pca'
        mth2='ica'
    else:
        mth1='tsne'
        mth2='mds'

    label_index = complete - 2

    if sum(nll[:, label_index]) == 0:
        print("Fucked up labels.")
        return

    plt.subplot(221)
    scatter_signals(nll[:, :-2],
                    nll[:, label_index],
                    dengue=True,
                    show=False,
                    legend=False,
                    title="Sem Bloqueio",
                    multi=True,
                    method=mth1)

    plt.subplot(222)
    scatter_signals(blk[:, :-2],
                    blk[:, label_index],
                    dengue=False,
                    show=False,
                    legend=False,
                    title="Com Bloqueio",
                    method=mth1)


    plt.subplot(223)
    scatter_signals(nll[:, :-2],
                nll[:, label_index],
                dengue=True,
                show=False,
                legend=False,
                method=mth2,
                multi=True)

    plt.subplot(224)
    scatter_signals(blk[:, :-2],
                    blk[:, label_index],
                    dengue=False,
                    show=False,
                    legend=False,
                    method=mth2)

    if write:
        comp_status = 'comp' if complete else 'incomp'
        fn = f'assets/{up_status}-{comp_status}-{mth_set}.png'
        plt.savefig(fn, format='png')

    plt.close()

def comparison_hplot(nll, blk, write=False, order=True):

    if order:

        nll = nll[nll[:,-2].argsort()]
        blk = blk[blk[:,-2].argsort()]

    plt.subplot(221)

    similarity_plot(nll, title="Sem Bloqueio")#, orientation=None)

    plt.subplot(222)

    similarity_plot(blk, title="Com Bloqueio")

    if write:
        fn = f'assets/heat.png'
        plt.savefig(fn, format='png')

    plt.show()

    plt.close()

null = np.load("signals/raw_signals.npy")
null = np.concatenate((null, np.zeros((len(null), 1))), axis=1)
null_up = np.load("signals/resampled_raw_signals.npy")

block = np.load("signals/new_trimmed_signals.npy")
block_up = np.load("signals/new_upsignals.npy")