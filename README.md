# Classificacao-Resposta-Chip

Repositório privado voltado a resolução do problema de classificação de respostas
de um sistema eletrônico cujas propriedades analíticas são desconhecidas. O problema
é uma dicotomia N:1. Assume-se por ora invariância temporal. Abordagens inicias
serão neurais, documentadas brevemente neste documento e a fundo em outros. 
