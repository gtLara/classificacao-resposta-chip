"""
Conjunto de ferramentas de visualização. Desenvolvidas continuamente durante o
projeto.

Gabriel Lara
"""
import matplotlib.pyplot as plt
import numpy as np
from sklearn.manifold import TSNE
from sklearn.manifold import MDS
from sklearn.decomposition import PCA
from sklearn.decomposition import FastICA
from mpl_toolkits.mplot3d import Axes3D
from scipy.spatial import distance
import torch

def plot_signals(signals, labels=None, title=None, sep=False, save=False, fp="assets/visual/simple/", show=True):


    c={ 0:'black',
        1:'red',
        2:'green',
        3:'blue'
        }

    t={
        0:'Aptâmero',
        1:'Zika',
        2:'Dengue',
        3:'CEA'
        }

    if sep:
        n_class = len(set(labels))
        for n in range(n_class):
            for signal in signals[np.where(labels==n)]:
                plt.plot(signal, color=c[n])
                plt.title(t[n])

            if save: plt.savefig(fp+t[n]+"signals.png")
            if show: plt.show()

    else:

        labels = [int(label) for label in labels]

        for s, signal in enumerate(signals):
            if labels is not None:
                plt.plot(signal, color=c[labels[s]])
            else:
                plt.plot(signal, color='black')

        plt.title(title)

        if save:
            plt.savefig(fp+"signals.png")

        if show: plt.show()

def scatter_signals(signals, labels, method='tsne', dims=2, title=None, legend=True, dengue=False, show=True, complete=True, save=False, fp="assets/visual/partial/"):

    if not isinstance(labels, list):
        labels = list(labels)

    if not isinstance(labels[0], np.int64) and not isinstance(labels[0], int):
        try:
            labels = [label[0] for label in labels]
        except:
            pass

    labels = [l for l in labels]

    size = len(labels)

    ap = [index for index in range(size) if labels[index] == 0]

    zk = [index for index in range(size) if labels[index] == 1]

    if dengue:
        dg = [index for index in range(size) if labels[index] == 2]
        cea = [index for index in range(size) if labels[index] == 3]
        neg = ap + cea + dg

    else:
        cea = [index for index in range(size) if labels[index] == 2]
        neg = ap + cea

    reduce = {
                'tsne': TSNE,
                'mds': MDS,
                'pca': PCA,
                'ica': FastICA
             }

    reduced_signals = reduce[method](n_components=dims).fit_transform(signals)


    if dims == 3:

        fig =  plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        ax.scatter(reduced_signals[:, 0],
                   reduced_signals[:, 1],
                   reduced_signals[:, 2])
                   #c = cols)

        print("Ainda nao implementado")
        return

    if not complete:

        plt.scatter(reduced_signals[neg, 0],
                    reduced_signals[neg, 1],
                    c = 'black',
                    label = 'Negativo')

        plt.scatter(reduced_signals[zk, 0],
                    reduced_signals[zk, 1],
                    c = 'red',
                    label = 'Positivo')

    else:

        plt.scatter(reduced_signals[ap, 0],
                    reduced_signals[ap, 1],
                    c = 'black',
                    label = 'Aptâmero')

        plt.scatter(reduced_signals[zk, 0],
                    reduced_signals[zk, 1],
                    c = 'red',
                    label = 'Zika')

        plt.scatter(reduced_signals[cea, 0],
                    reduced_signals[cea, 1],
                    c = 'blue',
                    label = 'CEA')

        if dengue:

            plt.scatter(reduced_signals[dg, 0],
                reduced_signals[dg, 1],
                c = 'green',
                label = 'Dengue')


    if legend:plt.legend()

    plt.title(title)

    plt.yticks([])
    plt.xticks([])

    if save: plt.savefig(fp+"scatter-"+str(complete).lower())
    if show: plt.show()

def similarity(A_vecs, B_vecs, metric = 'euclidian'):

    s = A_vecs.shape[0]
    c = B_vecs.shape[0]

    similarity = np.zeros((s, c))

    for si, svec in enumerate(A_vecs):
        for ci, cvec in enumerate(B_vecs):
            similarity[si, ci] = distance.euclidean(svec, cvec)

    return similarity

def similarity_plot(a, b=None, show=False, xl=None, yl=None, title=None, sim=None, cmap='bone', save=False, fn='simplot.png', orientation='vertical'):

    if b is None:
        b = a

    if sim is None:
        sim = similarity(a, b)

    plt.imshow(sim, cmap=cmap)

    plt.title(title)
    plt.xlabel(xl)
    plt.ylabel(yl)
    if orientation is not None: plt.colorbar(orientation=orientation)

    plt.yticks([])
    plt.xticks([])

    if save: plt.savefig(fn)
    if show: plt.show()

def er_plot(er_tracker, verification_range, title=None, write=False):
    rej = [item[1] for item in er_tracker]
    acc = [item[0] for item in er_tracker]

    cross = [abs(item[0] - item[1]) for item in er_tracker]
    eer_index = np.argmin(cross)
    eer = rej[eer_index]

    plt.plot(verification_range, rej, c='black', label='Falso Negativo')
    plt.plot(verification_range, acc, c='red', label='Falso Positivo')
    #plt.scatter(eer_index, eer, c='black')
    optimal = f'({verification_range[eer_index]:.3}, {eer})'
    plt.text(verification_range[eer_index], eer+(0.08), s=optimal)

    plt.legend()

    plt.show()

    if write:
        fn = 'EER.png'
        plt.savefig(fn, format='png')

    return verification_range[eer_index]

def loss_plot(loss, col='r', title = 'Loss por Iteração'):
    plt.title(title)
    plt.plot(loss, color = col)
    plt.show()

def confusion_2d(case_counter, n_cases, title = 'Matriz de Confusão: Verdade x Predição'):

    cases_counter = case_counter

    for case in case_counter:
        cases_counter[case] = round(case_counter[case]/n_cases, 2)


    confusion = np.ones((2, 2), dtype=float)

    class_dict = {
                  (0, 0): 'tn',
                  (0, 1): 'fp',
                  (1, 0): 'fn',
                  (1, 1): 'tp',
                  }

    for k in class_dict.keys():
        confusion[k[0], k[1]] = case_counter[class_dict[k]]

    fig, ax = plt.subplots()
    im = ax.imshow(confusion, cmap='Wistia')

    plt.title(title)

    ax.set_xticks(np.arange(2))
    ax.set_yticks(np.arange(2))

    ax.set_xticklabels(['Negativo', 'Positivo'])
    ax.set_yticklabels(['Negativo', 'Positivo'])

    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    for i in range(2):
        for j in range(2):
            text = ax.text(j, i, case_counter[class_dict[(i, j)]],
                           ha="center", va="center", color="black")

    plt.show()

def study_representations(model, partition, visual=True, dims=3, method='tsne', dengue=False, title=None):

    model.expose = True

    vecs = np.zeros((1, model.k+1))

    with torch.no_grad():

        for signals, labels in partition:

            signals = signals.to(torch.device('cpu'))
            labels = labels.view(len(labels), -1).detach().numpy()
            outputs = model(signals.float())

            vecs = np.vstack((vecs, np.concatenate((outputs, labels), axis=1)))

        if visual:
            scatter_signals(vecs[:, 0:-1], vecs[:, -1], dims=dims, title=title, method=method, dengue=dengue)

    return vecs
