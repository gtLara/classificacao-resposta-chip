from utils.visualization import confusion_2d, loss_plot
import torch
from collections import Counter
import numpy as np

def cd(label):
    return int(label == 1)

def verify_batch(verifier,
                test_loader, 
                visual=True,
                thresh=None,
                vclass=1,
                validate=False,
                device=torch.device('cpu')):
        
    if thresh is not None:
        verifier.threshold = thresh
    
    class_dict = {
              (0, 0): 'tn',
              (0, 1): 'fp',
              (1, 0): 'fn',
              (1, 1): 'tp',
              }

    cases = []
    fps = []
    
    
    with torch.no_grad():
        n_correct = 0
        n_samples = 0
        for signals, labels in test_loader:
            signals = signals.to(device)
            labels = labels.to(device)
                        
            embeddings = verifier.encoder(signals.float())
            predicted = [int(verifier.verify(emb.view(1, -1))) for emb in embeddings]
                        
            for y, y_hat in zip(labels.numpy(), predicted):
                case = class_dict[(cd(y), cd(y_hat))]
                cases.append(case)
            
    case_counter = Counter(cases)   
    
    if visual:
        confusion_2d(case_counter, n_cases=len(cases))
    
    if validate:
        precision = round(case_counter['tp'] / (case_counter['tp'] + case_counter['fp']), 2)
        recall = round(case_counter['tp'] / (case_counter['tp'] + case_counter['fn']), 2)
        F1 = round(2 * (precision*recall)/(precision+recall), 2)
        
        print(f'F1: {F1:}')
        print(f'precision: {precision}')
        print(f'recall: {recall}')
        
        return F1, precision, recall
    
    attempts = len(case_counter)
    
    return ((case_counter['fp']/attempts), (case_counter['fn']/attempts))
    
    
    
    

