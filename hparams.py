"""
Classes usadas para guardar hiperparametros dos diferentes modelos.

Gabriel Lara
"""

class MLPNullParams:
    def __init__(self, n_features):

        self.batch_size = 64
        self.test_split = 0.2
        self.shuffle_data = True
        self.r_seed = 42

        self.hid_1_size = int(round(n_features * 1.5, 1))
        self.hid_2_size = int(round(n_features * 1.8, 1))
        self.hid_3_size = int(round(n_features * 2, 1))
        self.hid_4_size = int(round(n_features * 2.3, 1))
        self.hid_5_size = int(round(n_features * 2.5, 1))
        self.hid_6_size = int(round(n_features * 3, 1))

        self.out_size = 2

        self.n_epochs = 2500
        self.lrate = 1e-4


class EncoderParams:
    def __init__(self, n_features, n_samples, k=256):

        self.batch_size = n_samples
        self.test_split = 0.2
        self.shuffle_data = True
        self.r_seed = 42

        self.in_size = n_features
        self.hid_1_size = int(round(n_features * 1.3, 1))
        self.hid_2_size = int(round(n_features * 1.5, 1))
        self.hid_3_size = int(round(n_features * 1.8, 1))
        self.hid_4_size = int(round(n_features * 2, 1))
        self.k = k
        self.decay = 0.08


class ClassifierParams:
    def __init__(self, in_size, out_size):

        self.in_size = in_size
        self.hid_1_size = int(round(self.in_size * 1.5, 1))
        self.hid_2_size = int(round(self.in_size * 2, 1))
        self.out_size = out_size
