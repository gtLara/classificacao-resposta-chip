import torch
import sys
import torch.nn as nn
import numpy as np
from torch.utils.data import Dataset, DataLoader
from torch.utils.data.sampler import SubsetRandomSampler
from data_processing.data_processing import ResponseData, data_samplers
from tqdm import tqdm
from utils.visualization import study_representations, similarity_plot, er_plot
from models.signalseparator import Encoder, Classifier, Verifier
from models.mlp_null import FeedForwardNN
from hparams import EncoderParams, ClassifierParams, MLPNullParams
from utils.ge2e import GE2ELoss
from test_classification import dichotomy_test
from logger import Logger
from verify import verify_batch

device = torch.device('cpu')

log = True
test = True
study_reps = False
verify = False
verify_range = np.arange(0, 1.2, 0.001)
train_encoder = False
train_classifier = True

data = ResponseData("signals/resampled_raw_signals.npy", complete=True)

train_sampler, test_sampler = data_samplers(data.n_samples, tsplit=0.2)

hpe = EncoderParams(data.n_features, data.n_samples, k=100)
hpc = MLPNullParams(data.n_features)#ClassifierParams(hpe.k, 2)

train_loader = torch.utils.data.DataLoader(data, batch_size=hpe.batch_size,
                                           sampler=train_sampler)
test_loader = torch.utils.data.DataLoader(data, batch_size=hpe.batch_size,
                                          sampler=test_sampler)

encoder_epochs = 100#int(sys.argv[1])
classifier_epochs = 1500#int(sys.argv[2])

if train_encoder:

    ge2e_loss = GE2ELoss(init_w=10.0, init_b=-5.0, loss_method='softmax')

    encoder = Encoder(hpe.in_size,
                    hpe.hid_1_size,
                    hpe.hid_2_size,
                    hpe.hid_3_size,
                    hpe.hid_4_size,
                    hpe.k).to(device)

    encoder.train(encoder_epochs, train_loader, ge2e_loss)

if train_classifier:

    classifier = Classifier(hpc.in_size,
                            hpc.hid_1_size,
                            hpc.hid_2_size,
                            hpc.out_size)

    # classifier = FeedForwardNN(hpe.k, hpc.hid_1_size, hpc.hid_2_size, hpc.hid_3_size, hpc.hid_4_size, hpc.hid_5_size, hpc.hid_6_size, hpc.out_size)

    classifier_loss_tracker = []
    class_criterion = nn.CrossEntropyLoss()
    class_optimizer = torch.optim.Adam(classifier.parameters(),
                                 lr = 1e-4,
                                 weight_decay=0.02)


    for epoch in tqdm(range(classifier_epochs)):

        for i, (signals, labels) in enumerate(train_loader):

            #forward pass
            signals = signals.to(device).float()

            for l, label in enumerate(labels):
                labels[l] = float(label == 1)

            labels = labels.to(device)

            if train_encoder:

                embeddings = encoder(signals).to(device)

                outputs = classifier(embeddings)

            else:

                outputs = classifier(signals.to(device))


            loss = class_criterion(outputs, labels.long())

            classifier_loss_tracker.append(loss)

            #backpropagation

            class_optimizer.zero_grad()
            loss.backward()
            class_optimizer.step()

if verify:

    with torch.no_grad():

        for signals, labels in train_loader:
            vsignals = signals[np.where(labels==1)]

        vembeddings = encoder(vsignals.float())

        vcentroid = torch.mean(vembeddings, axis=0)

        verifier = Verifier(encoder, vcentroid)

    if verify_range is not None:

        eer_tracker = []

        for ct in verify_range:
            info = verify_batch(verifier, test_loader, visual=False, thresh=ct)
            eer_tracker.append(info)

        eer_t = er_plot(eer_tracker, verify_range)
        
        #adjusting to percentage
        
        verify_batch(verifier, test_loader, visual=True, thresh=eer_t, validate=True)

    else:

        info = verify_batch(verifier, test_loader, visual=False, thresh=eer_t, validate=True)

if test:

    f1, precision, recall = dichotomy_test(classifier,
                                            test_loader,
                                            # encoder=encoder,
                                            loss_tracker=classifier_loss_tracker
                                            )

    if log:

        if train_encoder:
            logger = Logger()
            logger.log([encoder_epochs, classifier_epochs, f1, precision, recall])
        else:
            logger = Logger(n_items=4, item_names=['Class Epochs', 'F1', 'Precision', 'Recall'], filepath="mlplog.txt")
            logger.log([classifier_epochs, f1, precision, recall])

mth = 'mds'

if study_reps:

    train_reps = study_representations(encoder,
                          train_loader,
                          visual = True,
                          dims = 2,
                          method = mth,
                          dengue = False
                          )

    test_reps = study_representations(encoder,
                          test_loader,
                          visual = True,
                          dims = 2,
                          method = mth,
                          dengue = False
                          )

    train_reps = train_reps[np.argsort(train_reps[:, -1])]
    test_reps = test_reps[np.argsort(test_reps[:, -1])]

    similarity_plot(train_reps[:, :-1])
    similarity_plot(test_reps[:, :-1])
