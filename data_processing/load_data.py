"""
Funções com objetivo de organizar dados em vetores rotulados a partir do
formato de txt enviados ao desenvolvidor dia 20/05/2020. Inclui funções de
visualização

Gabriel Lara @ 20/05/2020
"""
import unidecode
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from os import listdir as l
import pickle as pk

def dataload(pth, visual=False):
    "Carrega dados de observação única, permite visualização"

    if 'txt' not in pth: return

    df = pd.read_csv(pth, sep="	", header=None, names=['tempo', 'i'])
    i = df['i']

    if visual: plt.plot(df['tempo'], df['i'], "r+")

    return np.array(i)

def directory_dataload(drpth, key_1, key_2 = 'Aptâmero', write_data=False, positive=False, multiplot=False, write_fig=False):
    """
    Carrega dados de observação múltipla por escala.
    Permite salvar os dados rotulados e realizar visualização comparativa
    """

    scale = drpth.split("/")[-1].lower()

    comparative_tag = int(positive)

    files = l(drpth)

    ground = [f for f in files if key_2 in f]
    comparative = [f for f in files if key_1 in f]

    labeled_ground_values = [[dataload(drpth+'/'+pth), 0] for pth in ground]
    labeled_comparative_values = [[dataload(drpth+'/'+pth), comparative_tag]  for pth in comparative]

    key_1 = unidecode.unidecode(key_1)
    key_2 = unidecode.unidecode(key_2)

    if multiplot:
        plt.clf()
        for v, value in enumerate(labeled_comparative_values):
            plt.plot(value[0], color='black')
            plt.plot(labeled_ground_values[v][0], 'r+')
            plt.title(key_1+" "+scale)

        if write_fig:
            plt.savefig(drpth.lower()+'.svg', format='svg')

    if write_data:
        with open(drpth+'/'+key_2.lower(), "wb") as pf:
            pk.dump(labeled_ground_values, pf)
        with open(drpth+'/'+key_1.lower(), "wb") as pf:
            pk.dump(labeled_comparative_values, pf)

    return labeled_ground_values, labeled_comparative_values


def collective_directory_dataload(drpth, key='Sem_Tratamento', write_data=False, fn=None):
    """
    Versão alternativa de directory dataload
    """

    label = {
            'Aptâmero':0,
            'Zika':1,
            'CEA':2,
            }

    files = l(drpth)

    signals = [f for f in files if key in f]

    labeled_signals = np.zeros((len(signals), 78))

    for s, sig in enumerate(signals):

        labeled_signals[s, 0:-2] = dataload(drpth+'/'+sig)[4:]
        labeled_signals[s, -1] = label[sig.split("_")[2]]
        labeled_signals[s, -2] = int('Zika' in sig)

    if write_data:
        assert fn != None
        np.save(fn, labeled_signals)