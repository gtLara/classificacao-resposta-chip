"""
Processa comunicação entre dados numpy e classes do PyTorch. Implementa
decisões sobre completude ou não dos rótulos nos dados de entrada.
"""
import numpy as np
from torch.utils.data import Dataset
from torch.utils.data.sampler import SubsetRandomSampler
import torch

class ResponseData(Dataset):

    def __init__(self, dpath = 'raw_signals.npy', complete=False):

        data = np.load(dpath)

        if complete:
            X, y, y_complete = data[:, :-2], data[:, -2], data[:, -1]
            self.y = torch.from_numpy(y_complete)
        else:
            X, y = data[:, :-2], data[:, -2]
            self.y = torch.from_numpy(y)

        self.n_samples = X.shape[0]
        self.n_features = X.shape[1]
        self.X = torch.from_numpy(X)
        self.shape = X.shape

    def __getitem__(self, index):

        return self.X[index], self.y[index]

    def __len__(self):
        return self.n_samples

def data_samplers(size, tsplit = 0.2, seed=23, shuffle_data = True):

    indices = list(range(size))
    split = int(np.floor(tsplit * size))

    if shuffle_data:
        np.random.seed(seed)
        np.random.shuffle(indices)

    train_indices, test_indices = indices[split:], indices[:split]

    tr_sampler = SubsetRandomSampler(train_indices)
    te_sampler = SubsetRandomSampler(test_indices)

    return tr_sampler, te_sampler
