"""
Funções que traduzem pickles de dados em txt em vetores numpy. Usa-se esse
momento do processamento para fazer uma tratamento básico dos dados.
"""
import numpy as np
import pickle as p
from mpl_toolkits.mplot3d import Axes3D
from imblearn.over_sampling import SMOTE
from utils.visualization import plot_signals, scatter_signals

def pull_label(signal):

    if signal in ap_arr:
        return 0

    if signal in zika_arr:
        return 1

    if signal in dengue_arr:
        return 2

    if signal in cea_arr:
        return 3

with open("data/19-05-2020/null/cea/Sem Tratamento/aptamero", "rb") as pf:
    amp_cea = p.load(pf)

with open("data/19-05-2020/null/cea/Sem Tratamento/cea", "rb") as pf:
    cea = p.load(pf)

with open("data/19-05-2020/null/zika/Sem Tratamento/aptamero", "rb") as pf:
    amp_zika = p.load(pf)

with open("data/19-05-2020/null/zika/Sem Tratamento/zika", "rb") as pf:
    zika = p.load(pf)

with open("data/19-05-2020/null/dengue/Sem Tratamento/aptamero", "rb") as pf:
    amp_dengue = p.load(pf)

with open("data/19-05-2020/null/dengue/Sem Tratamento/dengue", "rb") as pf:
    dengue = p.load(pf)

dengue_arr = np.array([l[0] for l in dengue])
zika_arr = np.array([l[0] for l in zika])
cea_arr = np.array([l[0] for l in cea])

ap_arr = np.vstack((np.array([l[0] for l in amp_cea]), np.array([l[0] for l in amp_dengue])))

raw_signals = np.vstack([zika_arr, dengue_arr, cea_arr, ap_arr])

#"cabeçalho" dos sinais retirado

trimmed_signals = raw_signals[:, 4:]

N = len(trimmed_signals)

complete_labels = np.array([pull_label(signal) for signal in raw_signals]).reshape((N, 1))

labels = np.zeros((N,1), dtype=int)
labels[:23] = 1

original_data = np.concatenate((trimmed_signals, labels), axis=1)
original_data = np.concatenate((original_data, complete_labels), axis=1)
original_data = original_data.astype(float)

np.save("raw_signals.npy", original_data)

smote = SMOTE(random_state=23)

upsampled_trimmed_signals, upsampled_labels = smote.fit_resample(trimmed_signals, labels)

upsampled_labels = upsampled_labels.reshape(188, 1)

upsampled_complete_labels = [label for label in complete_labels]

for i in range(len(upsampled_labels) - len(upsampled_complete_labels)):

    upsampled_complete_labels.append(1)

upsampled_complete_labels = np.array(upsampled_complete_labels).reshape(188, 1)

data = np.concatenate((upsampled_trimmed_signals, upsampled_labels), axis=1)
data = np.concatenate((data, upsampled_complete_labels), axis=1)
data = data.astype(float)

np.save("resampled_raw_signals", data)

#visualização

plot_labels = [label for label in upsampled_labels]

for s, signal in enumerate(upsampled_trimmed_signals):
    if signal not in trimmed_signals:
        plot_labels[s] = 2

complete_labels = [int(label) for label in complete_labels]
plot_signals(trimmed_signals, complete_labels)
