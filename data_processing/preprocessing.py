"""
Arquivo que processa dados txt para vetores rotulados

Gabriel Lara @ 20/04/2020
"""
#"../data/null/cea/Subtracao"
from load_data import directory_dataload
from os import listdir as l

file_id = {
           'cea':'CEA',
           'zika':'Zika',
           'dengue':'Dengue',
            }

root = '../data/'

current_load = root+'bsa_2h' #primeira leva de dados a serem estudados

dirs = l(current_load)
dirs.remove('zika-dengue')

for d in dirs:
    for scale_dir in l(current_load+'/'+d):

        tag = False

        if d=='zika':
            tag = True

        directory_dataload(current_load+'/'+d+'/'+scale_dir, key_1=file_id[d],
                           write_data=True, multiplot=True, write_fig=True,
                           positive=tag)