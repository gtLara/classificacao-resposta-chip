from utils.visualization import confusion_2d, loss_plot
import torch
from collections import Counter
from logger import Logger

def cd(label):
    return int(label == 1)

def dichotomy_test(model,
                   test_loader,
                   encoder=None,
                   loss_tracker=None,
                   visual = True,
                   device = torch.device('cpu')):

    class_dict = {
              (0, 0): 'tn',
              (0, 1): 'fp',
              (1, 0): 'fn',
              (1, 1): 'tp',
              }

    cases = []
    fps = []


    with torch.no_grad():
        n_correct = 0
        n_samples = 0
        for signals, labels in test_loader:
            signals = signals.to(device)
            labels = labels.to(device)

            if encoder:
                outputs = model(encoder(signals.float()))
            else:
                outputs = model(signals.float())
            # max returns (value ,index)
            _, predicted = torch.max(outputs.data, 1)
            n_samples += labels.size(0)
            n_correct += (predicted == labels).sum().item()

            for y, y_hat in zip(labels.numpy(), predicted.numpy()):
                case = class_dict[(cd(y), cd(y_hat))]
                cases.append(case)

                if case == 'fp': fps.append(y)

    case_counter = Counter(cases)
    fp_counter = Counter(fps)

    if visual:
        loss_plot(loss_tracker)
        confusion_2d(case_counter, n_cases=len(cases))

    precision = round(case_counter['tp'] / (case_counter['tp'] + case_counter['fp']), 2)
    recall = round(case_counter['tp'] / (case_counter['tp'] + case_counter['fn']), 2)

    if case_counter['tp'] != 0:
        F1 = round(2 * (precision*recall)/(precision+recall), 2)
        print(f'F1: {F1:}')

    print(f'precision: {precision}')
    print(f'recall: {recall}')

    return F1, precision, recall