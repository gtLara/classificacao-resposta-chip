import os
#import pandas as pd

class Logger():

    def __init__(self,
                 n_items=5,
                 item_names=['Enc Epochs', 'Class Epochs', 'F1', 'Precision', 'Recall'],
                 filepath='log.txt',
                 sep = ' | '):

        self.sep = sep
        self.file = filepath
        self.n_items = n_items

        if item_names:


            self.header = self.format(item_names)

            if not os.path.isfile(filepath):
                with open(filepath, 'w') as f:
                    f.write(self.header)

    def format(self, item_list):
        item_list = [str(item) for item in item_list]
        data = item_list[0]
        for i, item in enumerate(item_list[1:]):
            data += (self.sep+item)

        data += '\n'

        return data


    def log(self, data):

        if len(data) != self.n_items:
            print("Number of items is unmatched. There may be missing results.")

        data = self.format(data)

        with open(self.file, "a") as f:
            f.write(data)

#TODO   def make_df(self):


